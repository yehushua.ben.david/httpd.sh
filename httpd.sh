#!/bin/bash
################################################################################
#
# httpd.sh is a flask/FastAPI/bottle like for bash.
# Base on Ncat, httpd.sh takes care of the routes dispatching.
# Git: https://gitlab.com/yehushua.ben.david/httpd.sh
#
################################################################################
export VERSION=1.0.3
export DEFAULT_PORT=8080
export ROOT=$(realpath .)
VERBOSE=0

function 200_OK() {
	echo -e "HTTP/1.1 200 OK\nContent-Type: ${1:-text/plain}\n"
}

function 404_KO() {
	echo -e "HTTP/1.1 404 Vincent Vega\nContent-Type: text/plain\n
[404]: $1 - Vincent Vega is lost."
	return 1
}

function GET_favicon.ico() {
	200_OK image/ico
	echo "H4sIAAAAAAAAA3P3dLOwTBRgEGD4yAACNx69BtMMij9ZOKMYmBgU/3P7uYYEOzsGuBrpGTAzAqV0QPIgLQxMz6eoKAFBi4AThyKY4GhSwIoEOFiYgLo4GJABBwsSB2QcLh6IiwoEMEXw8QXQ5cH6BUAAlw1IgAsSHEZAJw0bv+PTjgyAfrcGAAPEzR8lAgAA" |
		base64 -d | gzip -d
}

function GET_() {
	200_OK text/html
	echo "<h1> It works !</h1>"
}

function log() {
	echo -e "\e[38;2;255;255;0;1m$*\e[m" >&2
}

function fileserver() {
	TARGET="${1#*/}"

	function fstat() {
		stat --printf="<td>%A</td> <td>%U</td> <td>%G</td> <td>%s</td>" $1
	}
	if [ -f ./$TARGET ]; then
		200_OK "$(type_mime ./$TARGET)"
		cat ./$TARGET
	fi
	if [ -d ./$TARGET ]; then
		200_OK "text/html"
		echo "<html><body>"
		cd ./$TARGET
		FPATH="/$TARGET/"
		FPATH=${FPATH//\/\//\/}
		echo "<a href='${FPATH}..'><b>$FPATH..</b></a><hr><table>"
		for f in *; do
			echo "<tr>$(fstat $f)<td><a href='$FPATH$f'>$f</a></td></tr>"
		done
		echo "</table></body></html>"
	fi

}

# Check for --help, --port and --version arguments
for arg in "$@"; do
	case $arg in
	-h | --help)
		echo "Usage: httpd.sh [OPTION]..."
		echo "Run a simple Bash web server."
		echo ""
		echo "Options:"
		echo "  --root=PATH_DOCUMENT_ROOT"
		echo "                Path of the working directory."
		echo "  --help        display this help and exit."
		echo "  --version     output version information and exit."
		echo "  -v, --verbose define verbose logging mode."
		echo "  -p PORT, --port=PORT"
		echo "                specify the port for the web service to run on (default: 8080)."
		echo ""
		echo "Report bugs to: https://gitlab.com/yehushua.ben.david/httpd.sh"
		exit 0
		;;
	--version)
		echo "httpd.sh version $VERSION"
		exit 0
		;;
	-p | --port)
		DEFAULT_PORT="${2}"
		shift 2
		;;
	-r | --root)
		ROOT="${2}"
		shift 2
		;;
	-r*)
		ROOT="${arg#-p}"
		shift
		;;
	-p*)
		DEFAULT_PORT="${arg#-p}"
		shift
		;;
	--port=*)
		DEFAULT_PORT="${arg#*=}"
		shift
		;;
	--root=*)
		ROOT="${arg#*=}"
		shift
		;;
	-v | --verbose)
		VERBOSE=1
		shift
		;;
	esac
done
function urldecode() {
	: "${*//+/ }"
	echo -e "${_//%/\\x}"
}

function type_mime() { xdg-mime query filetype "$1"; }

function read_headers() {
	while read ROW; do
		if [[ ${#ROW} -eq 1 && "$(printf '%0.2X' "'$ROW")" == "0D" ]]; then
			break
		fi
		HEADERS+="$ROW
"
	done
}
if [ "$1" = "routes" ]; then
	ta=$(date +%s%N)
	export DEFAULT_PORT="$2"
	export VERBOSE="$3"
	export ROOT="$4"
	export HTTPD="$5"
	export HEADERS
	export -f 200_OK
	export -f urldecode
	export -f type_mime
	export -f log
	export -f fileserver
	cd $ROOT
	read req

	verb="${req%% *}"
	route="${req% HTTP/*}"

	route="${route%\?*}"
	route="${route%@*}"

	route="${route#*/}"
	while [ "$route" != "${route%%/*}" ]; do
		path="${route%/*}"
		candidate="$path/${verb}_${route##*/}.sh"
		if [ -f "$candidate" ]; then
			HANDLER="${verb}_${route##*/}.sh"
			break
		fi
		candidate="$path/${verb}_.sh"
		if [ -f "$candidate" ]; then

			HANDLER="${verb}_${route##*/}.sh"
			break
		fi
		route="${path}"
	done
	if [ "$HANDLER" ]; then
		HANDLER="bash $(realpath $candidate)"
		cd $path
	else
		HANDLER="${verb}_$route"
		if [ -f "${HANDLER}.sh" ]; then
			HANDLER="bash ${HANDLER}.sh"
		else
			HANDLER="${verb}_"
			if [ -f "${HANDLER}.sh" ]; then
				HANDLER="bash ${HANDLER}.sh"
			else
				HANDLER="404_KO"
			fi
		fi
	fi
	read_headers
	if [[ $VERBOSE -eq 1 ]]; then
		$HANDLER "${req% HTTP/*}" "$HEADERS"
	else
		$HANDLER "${req% HTTP/*}" "$HEADERS" 2>/dev/null
	fi
	EXIT_CODE=$?
	case $EXIT_CODE in
	0) STATE=200 ;;
	*) [[ "$HANDLER" == "404_KO" ]] && STATE=404 || STATE="ERROR_$EXIT_CODE" ;;
	esac

	tb=$(date +%s%N)
	td=$(($tb - $ta))
	[[ $VERBOSE -eq 1 ]] &&
		echo "[$(date)][$NCAT_REMOTE_ADDR][$STATE][$(printf "%'d ms" $((td / 1000000)))] $req" >&2
else
	echo "Start httpd.sh $VERSION on ${DEFAULT_PORT}"
	echo "  DocRoot: $ROOT"
	echo "  VERBOSE: $VERBOSE"
	ncat -kl -p ${DEFAULT_PORT} -c "$0 routes ${DEFAULT_PORT} $VERBOSE '$ROOT' '$(realpath $0)'"
fi
