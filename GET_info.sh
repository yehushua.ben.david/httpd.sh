200_OK text/html
cat <<DONE
<style>
pre {
  background-color : rgb(220,220,220);
 margin:0px;
 padding:5px ;
 white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */
}
</style>
<table border=1 style="word-break: break-word ;table-layout: fixed;width:100%">
<tr><td><b>http.sh</b> Version: <b>$VERSION </b> Listen : <b>${DEFAULT_PORT}</b>
 in <b> $PWD </b>
</td></tr>
<tr><td><b>Binary</b></td><tr>
<tr><td><pre>$HTTPD</pre></td><tr>
<tr><td><b>Help</b></td><tr>
<tr><td><pre>$($HTTPD --help)</pre></td></tr>
<tr><td><b>OS Info</b></td></tr><tr><td><pre>$(cat /etc/os-release)</pre></td></tr>
<tr><td><b>env</b></td></tr><tr><td><pre>$(env)</pre></td></tr>
<tr><td><b>Request</b></td></tr><tr><td><pre>$1<br>$2</pre></td></tr>
</table>
DONE
