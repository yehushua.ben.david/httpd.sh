FROM ubuntu
LABEL name="BASH HTTPD"
RUN apt update
RUN apt install -y vim ncat git
RUN apt install -y xdg-utils
RUN mkdir -p /www
WORKDIR /app
COPY httpd.sh /app/
RUN chmod +x /app/httpd.sh
EXPOSE 8080
CMD ["/app/httpd.sh","-v", "--root","/www"]
