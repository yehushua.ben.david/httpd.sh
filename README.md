# httpd.sh

httpd.sh is a flask/FastAPI/bottle-like script written in bash. It's based on Ncat and takes care of route dispatching.

## Version

Current version is 1.0.3

## Usage

```bash
./httpd.sh [OPTION]...
```

This runs a simple Bash web server.

### Options
- `--root=PATH_DOCUMENT_ROOT` : Path of the working directory.

- `--help`     : display this help and exit
- `--version`  : output version information and exit
- `-v, --verbose` : define verbose logging mode
- `-p PORT, --port=PORT` : specify the port for the web service to run on (default: 8080)

## Bug Report

Report bugs to: https://gitlab.com/yehushua.ben.david/httpd.sh

## License

This project is licensed under the Free To Use License. It guarantees end users the freedom to run, study, share, and modify the software.

```
Anybody is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.
```

Please see the [LICENSE](LICENSE) file for the full text.

## Author

httpd.sh was written by [Yehushua Ben David](https://gitlab.com/yehushua.ben.david)
